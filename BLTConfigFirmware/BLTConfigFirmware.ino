/************    Protocal of Serial Communication *************/
/****
  buf[0] = FE
  buf[1], buf[2]: size of command
  buf[3]: ADAVANCEDCMD
  buf[4]: BLTCONFIG
  buf[5],buf[6]: length of bluetooth name
  buf[7]~buf[6+namesize]: name of bluetooth
  buf[7+namesize]~buf[10+namesize]: pin of bluetooth
*/
#include <scp_basic_command.h>
#include <scp_system_command.h>
#include <scp_advanced_arduino_commands.h>
#include <scp_utilities.h>

// For Bluetooth Configuration
#include <SoftwareSerial.h>
#define NAMERESP 9
#define PINRESP  8
#define PINNUMBER 4
char namePrefix[] = "AT+NAME";
char pinPrefix[] = "AT+PIN";

//Firmware Version
#ifndef FMW_MAJOR
#define FMW_MAJOR 0
#endif
#ifndef FMW_MINOR
#define FMW_MINOR 7
#endif
#ifndef FMW_REVISION
#define FMW_REVISION 0
#endif

scpContext ctx;
SoftwareSerial mySerial(10,11);    // port11: RX, port10: TX

int serialSend(byte * data, int sz)
{
  return Serial.write(data, sz);
}
// write AT commands to bluetooth
void BLUAT_writecmd(char *cmd,int bytes) {
  // write command to set name
  int i=0;
  while(i < bytes){
    mySerial.write(cmd[i++]);
    delay(10);
  }
}
// command response from bluetooth
void CMDResp(char* cmdresp,int bytes) {
   int icb = 0;
   while(icb < bytes) {
    if(mySerial.available())
    {
      cmdresp[icb++] = mySerial.read();
    }
    delay(50);
  }
}
// use AT command to configure name of the bluetooth
void doBluetoothConfig(char *NAME,char *PIN,int name_size)
{
    /*******Set bluetooth name*******/
    char ATNAME[name_size + strlen(namePrefix)+1];
    // construct the name command
    strcpy(ATNAME,namePrefix);
    strncat(ATNAME,NAME,name_size); // form AT command of name
    int n_name = name_size + strlen(namePrefix);
    BLUAT_writecmd(ATNAME,n_name);
    delay(500);
    // read response from Bluetooth
    char NameResp[NAMERESP]; // array of response from bluetooth
    CMDResp(NameResp,NAMERESP);  
    delay(500);
    scpAdvancedArduinoReply(&ctx,FN_SUCCESS,NULL); 
    /*******Set bluetooth pin*******/
     char ATPIN[PINNUMBER+sizeof(pinPrefix)+1];
     strcpy(ATPIN,pinPrefix);
     strncat(ATPIN,PIN,PINNUMBER); // form AT command
     int n_pin = strlen(ATPIN);     
     BLUAT_writecmd(ATPIN,n_pin);
     delay(500);
     // read response from Bluetooth
     char PinResp[PINRESP]; // array of response from bluetooth
     CMDResp(PinResp,PINRESP);
     if(NameResp[0]=='O'&&NameResp[1]=='K'&&PinResp[0]=='O'&&PinResp[1]=='K')
        scpAdvancedArduinoReply(&ctx,FN_SUCCESS,NULL); 
     else
        scpAdvancedArduinoReply(&ctx,FN_FAILURE,NULL); 
}

/* PARSER */
int parse(uint8_t * buf, int sz) {
  if(buf[3] == SYSTEMCMD)
  {
    if(buf[4]==GETFMWINFO) {
       scpReportFMWVersion(&ctx, FMW_MAJOR, FMW_MINOR, FMW_REVISION);
    }
  }
  if(buf[3] == ADVANCEDCMD)
  {   
      if(buf[4]==BLTCONFIG){
        int name_size = mergeValueFromTwo7bitBytes(buf[5],buf[6]);
        // configuration information: size of name + bltname + pincode
        uint8_t *Pin = (uint8_t*)malloc(PINNUMBER*sizeof(uint8_t)); 
        uint8_t *Name = (uint8_t*)malloc((name_size)*sizeof(uint8_t));
        memcpy(Name,&buf[5+2],(name_size)*sizeof(uint8_t));
        memcpy(Pin,&buf[5+2+name_size],PINNUMBER*sizeof(uint8_t)); // 5: Head of command,2: name length
        doBluetoothConfig((char *)Name,(char *)Pin,name_size);
        free(Pin);
        free(Name);
     }
  }
  else {
    return 0;
  }
  return 1;
}

void setup() {
  Serial.begin(9600);
  mySerial.begin(9600); // configuration of extened serial ports: ~11: RX, ~10: TX
  while(!Serial){
    ; // wait for serial port to connect.
  }
  while(!mySerial){
    ; // wait for bluetooth serial port to connect.
  }
  scpSetSerialSendCallback(&ctx, serialSend);
  scpSetMsgParseCallback(&ctx, parse);
  //scpReportFMWVersion(&ctx, 0, 1, 0);
}

void loop() {
  while(Serial.available())
  {
    byte input = Serial.read();
    scpInputHandler(&ctx, &input, 1);
  }
}


